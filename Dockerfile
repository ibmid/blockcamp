FROM node:lts-alpine3.12

#TODO: complete Dockerfile to be able to run a containerized

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /home/node/app
COPY package*.json ./
USER node
RUN npm install
COPY --chown=node:node . .

CMD [ "yarn", "test" ]