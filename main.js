"use strict";

exports.sumFunction = (a, b) => {
  // TODO: return a + b
  var sum = a + b;

  return sum;
};

exports.countWord = (text, word) => {
  // TODO: return the count of word occurence in the text (case insensitive)
  var parts = text.split(' ');
  var occur = 0;
  for (var i = 0; i < parts.length; i++) {
    if (parts[i].toLowerCase() == word.toLowerCase()) {
      occur++;
    }
  }

  return occur;
};

exports.findMissingNumber = (arrayNumber) => {
  // TODO: return the missing number in the array of 1 to 100
  var count = 1;
  for (let k = 1; k <= 100; k++) {
    if (arrayNumber[k - 1] != count) {
      break;
    }
    count++;
  }

  return count;
};

exports.findDuplicates = (arrayNumber) => {
  // TODO: return array of duplicates number
  var box = {}
  for (var i = 0; i < arrayNumber.length; i++) {
    if (box[arrayNumber[i]]) {
      box[arrayNumber[i]]++;
    } else {
      box[arrayNumber[i]] = 1;
    }
  }

  var ans = [];
  for (var con in box) {
    if (box[con] > 1) {
      ans.push(con);
    }
  }

  return ans;
};

exports.findIncorrects = (ref, arrayWord) => {
  // TODO: return incorrect NATO phonetic alpabhet
  var wrongs = [];
  for (var j = 0; j < arrayWord.length; j++) {
    var word = arrayWord[j];
    var alfOrder = word.charAt(0).charCodeAt(0) - 64;

    if (word != ref[alfOrder - 1]) {
      wrongs.push(word);
    }
  }

  return wrongs;
};